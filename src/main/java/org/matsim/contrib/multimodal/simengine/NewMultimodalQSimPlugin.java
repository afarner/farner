package org.matsim.contrib.multimodal.simengine;

import java.util.Collection;
import java.util.Collections;

import org.matsim.core.config.Config;
import org.matsim.core.mobsim.qsim.AbstractQSimPlugin;
import org.matsim.core.mobsim.qsim.interfaces.DepartureHandler;
import org.matsim.core.mobsim.qsim.interfaces.MobsimEngine;

public class NewMultimodalQSimPlugin extends AbstractQSimPlugin {
	public NewMultimodalQSimPlugin(Config config) {
		super(config);
	}

	public Collection<Class<? extends MobsimEngine>> engines() {
		return Collections.singleton(MultiModalSimEngine.class);
	}

	public Collection<Class<? extends DepartureHandler>> departureHandlers() {
		return Collections.singleton(MultiModalDepartureHandler.class);
	}
}
